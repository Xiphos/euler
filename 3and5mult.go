package main

/*
	Code for problem 1
	https://projecteuler.net/problem=1
	using worker pools to improve efficiency, shows time to execute
	as well as accepting arguments
*/

import(
	"fmt"
	"time"
	"github.com/jessevdk/go-flags"
	"github.com/schollz/progressbar"
)


func multiples(num, max int, results chan<- int){
	for i:= num ; i < max; i+= num{
		results <- i //Keep sending back multiples of the number
	}
	close(results)
}

func getaltsum(max int, verbosity int, progress bool) (int, time.Duration){
	sum := 0

	threes := make(chan int)
	fives := make(chan int)
	go multiples(3, max, threes)
	go multiples(5, max, fives)

	//Start up progress bar
	var totalPB *progressbar.ProgressBar
	if progress{
		totalPB = progressbar.New((max/5) + (max/3))
	}

	//Receive from the channels as long as at least one is open
	start := time.Now()
	for fives != nil || threes != nil{
		select{
			case val, ok := <- threes:
				//Accept from channel
				if val != 0{
					if verbosity > 1 && !progress{
						fmt.Printf("Received three: %v\n", val)
					}
					sum += val

					if progress{
						totalPB.Add(1)
					}
				}

				//Terminate channel
				if !ok{
					threes = nil
					if verbosity > 0{
						fmt.Println("Threes Complete")
					}
				}
			case val, ok := <- fives:
				//Accept from channel
				if val != 0{
					if verbosity > 1 && !progress{
						fmt.Printf("Received five: %v\n", val)
					}
					sum += val

					if progress{
						totalPB.Add(1)
					}
				}

				//Terminate channel
				if !ok{
					fives = nil
					if verbosity > 0{
						fmt.Println("Fives Complete")
					}
				}
		}
	}
	return sum, time.Since(start)
}

func getsum(max int, verbosity int, progress bool) (int, time.Duration){
	var sum = 0
	var pb *progressbar.ProgressBar

	if progress{
		pb = progressbar.New(max/5)
	}

	//Calculate the sum without concurrency
	start := time.Now()
	for i:=1; i < max; i++{
		if x:= i*3; x<max{
			sum += x
		}else{
			if verbosity > 1{
				fmt.Println("Threes Complete")
			}
		}
		if x := i*5; x<max{
			sum += x
		}else{
			if verbosity > 1{
				fmt.Println("Fives Complete")
			}
		}

		if progress{
			pb.Add(1)
		}
	}
	return sum, time.Since(start)
}

func main(){

	var options struct{
		Verbose []bool `short:"v" long:"verbose" description:"Show all multiples calculated"`
		ProgressBar bool `short:"p" long:"progress" description:"Show a progress bar"`
		Max int `short:"m" long:"maximum" default:"1000" description:"Set the maximum value to calculate to"`
		Alternate bool `short:"a" long:"alternate" description:"Run the program in alt mode (no concurrency)"`
		Repeat int `short:"r" long:"repeat" default:"1" description:"Repeat the program multiple times and print out the average time it took"`
		Quiet bool `short:"q" long:"quiet" description:"Suppress case output from repetition mode"`
	}

	_, err := flags.Parse(&options)
	if err != nil{
		return
	}

	start := time.Now() //We want to see how long the function took

	var sum = 0
	var max = options.Max  //Max number to sum multiples to
	var verbosity = len(options.Verbose)
	var showprogress = options.ProgressBar

	if options.Repeat < 1{
		fmt.Printf("Invalid repetitions given: %v\n", options.Repeat)
		return
	}
	
	if !options.Alternate{
		var times []time.Duration
		var elapsed time.Duration

		for i := 0 ; i < options.Repeat; i++{
			sum, elapsed = getaltsum(max, verbosity, showprogress)

			if options.Repeat > 1{
				if !options.Quiet{
					fmt.Printf("Trial %v | Sum: %v Time: %v\n", i+1, sum, elapsed)
				}
				times = append(times, elapsed)
			}
		}
		
		if options.Repeat > 1{
			fmt.Printf("Did %v runs in normal mode in an average time of %v\n", options.Repeat, timeAverage(times))
		}

	}else{
		var elapsed time.Duration
		var times []time.Duration

		for i:=0;i<options.Repeat;i++{
			sum, elapsed = getsum(max, verbosity, showprogress)

			if options.Repeat > 1{
				if !options.Quiet{
					fmt.Printf("Trial %v | Sum: %v Time: %v\n", i+1, sum, elapsed)
				}
				times = append(times, elapsed)
			}
		}

		if options.Repeat > 1{
			fmt.Printf("Did %v runs in alt mode in an average time of %v\n", options.Repeat, timeAverage(times))
		}

	}
	

	//We are done
	elapsed := time.Since(start)

	//Print the answer and the time it took
	fmt.Printf("\nSum of multiples of 3 and 5 up to %v: %v\n", max, sum)

	if options.Repeat == 1{
		fmt.Printf("Function completed in %v (concurrency mode: %v)\n", elapsed, options.Alternate)
	}
}


func timeAverage(t []time.Duration) time.Duration{
	var sum int64
	for _, cTime := range t{
		sum += cTime.Nanoseconds()
	}
	sum /= int64(len(t))
	return time.Duration(sum)
}

